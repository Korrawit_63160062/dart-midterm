import "dart:io";

List tokenizing(String exprs) {
  List symbols = ["+", "-", "*", "/", "%", "^", "(", ")"];
  List numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

  // Remove whitespace on front and back of string
  exprs = exprs.trim();

  // Remove duplicate whitespace
  final _whitespaceRE = RegExp(r"\s+");
  exprs = exprs.split(_whitespaceRE).join(" ");

  // Split adjacent operators
  symbols.forEach((i) {
    symbols.forEach((j) {
      exprs = exprs.replaceAll("$i$j", "$i $j");
    });
  });

  // Split adjacent operator and number
  numbers.forEach((i) {
    symbols.forEach((j) {
      // Split adjacent operator and number in case operator is on front
      exprs = exprs.replaceAll("$j$i", "$j $i");
      // Split adjacent operator and number in case number is on front
      exprs = exprs.replaceAll("$i$j", "$i $j");
    });
  });
  exprs = exprs.replaceAll(" ", "|");
  // Split adjacent operator and negative number
  numbers.forEach((i) {
    symbols.forEach((j) {
      if (j != ")") {
        exprs = exprs.replaceAll("$j|-|$i", "$j|- $i");
      }
    });
  });

  // Deal with exponent
  numbers.forEach((i) {
    numbers.forEach((j) {
      exprs = exprs.replaceAll("$i|^|$j", "$i ^ $j");
    });
  });

  exprs = exprs.replaceAll("||", "|");

  // Deal with negative symbol on exponent
  numbers.forEach((i) {
    numbers.forEach((j) {
      exprs = exprs.replaceAll("-|$i ^ $j", "- $i ^ $j");
      exprs = exprs.replaceAll("- |$i ^ $j", "- $i ^ $j");
      exprs = exprs.replaceAll("$i|^|-$j", "$i ^ - $j");
      exprs = exprs.replaceAll("$i|^|- $j", "$i ^ - $j");
      exprs = exprs.replaceAll("-|$i|^|-$j", "- $i ^ - $j");
    });
  });

  exprs = exprs.replaceAll("||", "|");

  // Deal with first negative number
  numbers.forEach((i) {
    if (exprs.substring(0, 3) == "-|$i") {
      exprs = exprs.replaceRange(0, 3, "- $i");
    }
  });

  exprs = exprs.replaceAll("||", "|");

  // Collect tokenizing words in list
  var split = exprs.split("|");

  //print(exprs);
  return split;
}

int checkPrecedenceLevel(String operator) {
  if (operator == "(" || operator == ")") {
    return 4;
  }
  if (operator == "^") {
    return 3;
  }
  if (operator == "*" || operator == "/") {
    return 2;
  }
  if (operator == "%") {
    return 3;
  }
  if (operator == "+" || operator == "-") {
    return 1;
  }
  return 0;
}

bool isInteger(String str) {
  if (str == null) {
    return false;
  }
  return double.tryParse(str) != null;
}

List toPostFix(List infix) {
  List operators = [];
  List postfix = [];
  infix = tokenizingExponent(infix);
  //infix = tokenizingNegative(infix);
  print("Your infix $infix");
  infix.forEach((i) {
    if (isInteger(i) == true) {
      // Add the token to the end of postfix
      postfix.add(i);
    } else {
      // For each token in the infix expression
      for (int j = 0; j < i.length; j++) {
        if (i[j] != " ") {
          // If the token is an integer Then
          if (isInteger(i[j]) == true) {
            // Add the token to the end of postfix
            postfix.add(i[j]);
          }

          // If the token is an operator Then
          if (isInteger(i[j]) == false && i[j] != "(" && i[j] != ")") {
            String str = i;
            if (str.contains("- ")) {
              postfix.add(i);
              break;
            }

            // While operators is not empty and
            //    the last item in operators is not an open parenthesis and
            //    precedence(token) < precedence(last item in operator) dp
            while (operators.isEmpty == false &&
                operators.last != "(" &&
                checkPrecedenceLevel(i[j]) <=
                    checkPrecedenceLevel(operators.last)) {
              postfix.add(operators.last);
              operators.removeLast();
            }
            // Add tpken to the end of operators
            operators.add(i[j]);
          }

          // If the token is an open parenthesis Then
          if (i[j] == "(") {
            // Add the token to the end of operators
            operators.add(i[j]);
          }

          // If the token is a close parenthesis Then
          if (i[j] == ")") {
            //print(operators);
            // While the last item in operators is not an open parenthesis do
            while (operators.last != "(") {
              // Remove the last item from operators and add it to postfix
              postfix.add(operators.last);
              operators.removeLast();
            }
            //Remove the open parenthesis from operators
            operators.removeLast();
          }
        }
      }
    }
  });
  // While operators is not the empty list do
  while (operators.isEmpty == false) {
    // Remove the last item from operators and add it to postfix
    postfix.add(operators.last);
    operators.removeLast();
  }

  return postfix;
}

List tokenizingExponent(List lst) {
  List tokenized = [];
  List tempList = [];
  for (int h = 0; h < lst.length; h++) {
    String temp = lst[h];
    if (temp.contains(' ^ ')) {
      temp = temp.replaceAll(" ^ ", "|^|");
      List tempList = temp.split("|");
      tempList.forEach((element) {
        tokenized.add(element);
      });
    } else {
      tokenized.add(temp);
    }
  }
  ;
  return tokenized;
}

List tokenizingNegative(List lst) {
  List tokenized = [];
  List tempList = [];
  for (int h = 0; h < lst.length; h++) {
    String temp = lst[h];
    if (temp.contains('- ')) {
      temp = temp.replaceAll("- ", "-|");
      List tempList = temp.split("|");
      tempList.forEach((element) {
        tokenized.add(element);
      });
    } else {
      tokenized.add(temp);
    }
  }
  ;
  return tokenized;
}

List tokenizingWhiteSpaceNegative(List lst) {
  List tokenized = [];

  for (int h = 0; h < lst.length; h++) {
    String temp = lst[h];
    if (temp.contains('- ')) {
      List tempList = [];
      temp = temp.replaceAll("- ", "-");
      tempList.add(temp);
      tempList.forEach((element) {
        tokenized.add(element);
      });
    } else {
      tokenized.add(temp);
    }
  }
  ;
  return tokenized;
}

double evaluatePostfix(List postfix) {
  postfix = tokenizingWhiteSpaceNegative(postfix);
  double result = 0;
  List values = [];
  double left, right;

  postfix.forEach((i) {
    if (isInteger(i) == true) {
      double temp = double.parse(i);
      values.add(temp);
    } else {
      right = values.last;
      values.removeLast();
      left = values.last;
      values.removeLast();
      result = calculator(left, right, i);
      values.add(result);
    }
  });
  return values.first;
}

double calculator(double left, double right, String operator) {
  double result = 0;
  switch (operator) {
    case "+":
      {
        result = add(left, right);
      }
      break;
    case "-":
      {
        result = substract(left, right);
      }
      break;
    case "*":
      {
        result = mutiply(left, right);
      }
      break;
    case "/":
      {
        result = divide(left, right);
      }
      break;
    case "%":
      {
        result = modulo(left, right);
      }
      break;
    case "^":
      {
        result = exponent(left, right);
      }
      break;
    default:
      {}
      break;
  }

  return result;
}

double add(double left, double right) {
  double result = left + right;
  return result;
}

double substract(double left, double right) {
  double result = left - right;
  return result;
}

double mutiply(double left, double right) {
  double result = left * right;
  return result;
}

double divide(double left, double right) {
  double result = left / right;
  return result;
}

double modulo(double left, double right) {
  double result = left % right;
  return result;
}

double exponent(double left, double right) {
  double expo = left;
  double result = 1;
  for (int i = 0; i < right; i++) {
    result = result * expo;
  }
  return result;
}

void main() {
  print('Input your infix expression :');
  String exprs = stdin.readLineSync() as String;
  List tokenizingExprs = tokenizing(exprs);
  List Postfix = toPostFix(tokenizingExprs);
  print("Your postfix : $Postfix");
  double result = evaluatePostfix(Postfix);
  print("Your result after evaluate postfix : $result");
}
